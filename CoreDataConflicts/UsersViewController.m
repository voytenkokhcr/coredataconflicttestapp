//
//  UsersViewController.m
//  CoreDataConflicts
//
//  Created by Войтенко Вадим on 13.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import "UsersViewController.h"
#import "UserViewController.h"
#import "User.h"
#import "AppDelegate.h"
#import "StoreCoordinatorCreator.h"

#import "Animal.h"
#import "Human.h"
#import "Device.h"

@interface UsersViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *addUserButtonItem;

@property (weak, nonatomic) User *selectedUser;

@property (strong, nonatomic) NSManagedObjectContext* context;

@property(strong,nonatomic) NSArray* allUsers;

@property(strong,nonatomic) NSPersistentStoreCoordinator *coordinator1;
@property(strong,nonatomic) NSPersistentStoreCoordinator *coordinator2;
@property(strong,nonatomic) NSPersistentStoreCoordinator *coordinator3;


@end

@implementation UsersViewController

static NSString *standardCellIdentifier = @"UserCell";

#pragma mark - User creating

- (IBAction)addUser:(id)sender {
    [self performSegueWithIdentifier:@"ShowUser" sender:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerNotifications];
    [self initMoc];
    [self updateAllUsers];
    
    // test different contexts
    [self testManageModels];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Table view

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.allUsers.count;
}

- (void) refreshData {
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    
    cell = [tableView dequeueReusableCellWithIdentifier:standardCellIdentifier forIndexPath:indexPath];
    
    User* user = [self.allUsers objectAtIndex:indexPath.row];
    
    
    for(UIView* cellSubview in cell.subviews) {
        for(UIView* cellViewSubview in cellSubview.subviews) {
            if([cellViewSubview isKindOfClass:[UILabel class]]) {
                ((UILabel*)cellViewSubview).text = user.fullName;
            }
        }
    }
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"allUsers";
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}



#pragma mark - Navigation

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if(self.selectedUser) {
        if(self.selectedUser) {
            ((UserViewController*)segue.destinationViewController).userName = self.selectedUser.fullName;
            
            // own magic
            
        }
    }
}

- (IBAction)unwindToUsers:(UIStoryboardSegue *)unwindSegue {
}

#pragma mark - Notification

- (void) registerNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"AddedSomeChangesToUser" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDidSaveNotification:) name:NSManagedObjectContextDidSaveNotification object:nil];
}

- (void) receiveNotification:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"AddedSomeChangesToUser"]) {
        
        
        self.context.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy;
        
        //no conflict error
        NSString *fullNameBefore = self.selectedUser.fullName;
        NSString *emailBefore = self.selectedUser.email;
        NSString *phoneNumberBefore = self.selectedUser.phoneNumber;
        
//        self.selectedUser.email = @"email@gmail.com";
        self.selectedUser.fullName = @"newFullName";
//        self.selectedUser.phoneNumber = @"212";
        
        NSError *error;
        if(![self.context save:&error]) {
            
            //conflict error
            NSLog(@"finish");
        }
        else {
            
            [self.context refreshObject:self.selectedUser mergeChanges:YES];
            
            NSString *fullNameAfter = self.selectedUser.fullName;
            NSString *emailAfter = self.selectedUser.email;
            NSString *phoneNumberAfter = self.selectedUser.phoneNumber;
            NSLog(@"finish");
        }
        
        
        [self updateAllUsers];
        [self.tableView reloadData];
    }
        
}


-(void) handleDidSaveNotification:(NSNotification *) notification {
    if(notification.object == self.context) {
        return;
    }
//    [self.context mergeChangesFromContextDidSaveNotification:notification];
    
    NSString *fullNameAfter = self.selectedUser.fullName;
    NSString *emailAfter = self.selectedUser.email;
    NSString *phoneNumberAfter = self.selectedUser.phoneNumber;
    NSLog(@"finish");
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedUser = [self.allUsers objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"ShowUser" sender:self];
}

#pragma mark - init

-(void) initMoc {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [self.context setPersistentStoreCoordinator:appDelegate.persistentStoreCoordinator];
}

-(void) updateAllUsers {
    
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"User"];
    
    NSError *error = nil;
    
    self.allUsers = [self.context executeFetchRequest:request error:&error];
}

#pragma mark - Multi Contexts

- (NSManagedObjectContext*) getManagedObjectContextForModelWithName:(NSString*) modelName {
    NSManagedObjectContext* context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    context.persistentStoreCoordinator = [ StoreCoordinatorCreator getStoreCoordinatorWithModelName:modelName];
    return context;
}

- (NSArray*) getAllObjectsWithClassName:(NSString*) className AndContext: (NSManagedObjectContext*) context {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:className];
    
    NSError *error = nil;
    
    return [context executeFetchRequest:request error:&error];
}

- (void) fillStorage1 {
    NSManagedObjectContext* context1 = [self getManagedObjectContextForModelWithName: @"TestDataModel1"];
    Animal* animal1 = [NSEntityDescription insertNewObjectForEntityForName:@"Animal" inManagedObjectContext:context1];
    animal1.name = @"Kesha";
    animal1.desc = @"Parrot";
    
    Animal* animal2 = [NSEntityDescription insertNewObjectForEntityForName:@"Animal" inManagedObjectContext:context1];
    animal2.name = @"Rex";
    animal2.desc = @"Dog";
    
    Animal* animal3 = [NSEntityDescription insertNewObjectForEntityForName:@"Animal" inManagedObjectContext:context1];
    animal3.name = @"Murzik";
    animal3.desc = @"Cat";
    if(![context1 save:nil]) {
        NSLog(@"Save failed in fillStorage1");
    }
}

- (void) fillStorage2 {
    NSManagedObjectContext* context2 = [self getManagedObjectContextForModelWithName: @"TestDataModel2"];
    Human* human1 = [NSEntityDescription insertNewObjectForEntityForName:@"Human" inManagedObjectContext:context2];
    human1.name = @"Vadim";
    human1.gender = @"man";
    
    Human* human2 = [NSEntityDescription insertNewObjectForEntityForName:@"Human" inManagedObjectContext:context2];
    human2.name = @"Vasya";
    human2.gender = @"man";
    
    Human* human3 = [NSEntityDescription insertNewObjectForEntityForName:@"Human" inManagedObjectContext:context2];
    human3.name = @"Anna";
    human3.gender = @"women";
    if(![context2 save:nil]) {
        NSLog(@"Save failed in fillStorage2");
    }
}

- (void) fillStorage3 {
    NSManagedObjectContext* context3 = [self getManagedObjectContextForModelWithName: @"TestDataModel3"];
    Device* device1 = [NSEntityDescription insertNewObjectForEntityForName:@"Device" inManagedObjectContext:context3];
    device1.type = @"Computer";
    device1.model = @"MacBook Pro";
    
    Device* device2 = [NSEntityDescription insertNewObjectForEntityForName:@"Device" inManagedObjectContext:context3];
    device2.type = @"Smartphone";
    device2.model = @"iPhone 6s";
    
    Device* device3 = [NSEntityDescription insertNewObjectForEntityForName:@"Device" inManagedObjectContext:context3];
    device3.type = @"Smartwatch";
    device3.model = @"Apple Watch";
    if(![context3 save:nil]) {
        NSLog(@"Save failed in fillStorage3");
    }
}

- (void) testManageModels {
    
    [self fillStorage1];
    [self fillStorage2];
    [self fillStorage3];
    
    NSManagedObjectContext* context1 = [self getManagedObjectContextForModelWithName: @"TestDataModel1"];
    NSManagedObjectContext* context2 = [self getManagedObjectContextForModelWithName: @"TestDataModel2"];
    NSManagedObjectContext* context3 = [self getManagedObjectContextForModelWithName: @"TestDataModel3"];
    
    NSArray* arr1 = [self getAllObjectsWithClassName:@"Animal" AndContext:context1];
    NSMutableArray* animalResultArray = [[NSMutableArray alloc] init];
    
    for(Animal* animal in arr1) {
        [animalResultArray addObject:animal.name];
        [animalResultArray addObject:animal.desc];
    }
    
    NSArray* arr2 = [self getAllObjectsWithClassName:@"Human" AndContext:context2];
    NSMutableArray* humanResultArray = [[NSMutableArray alloc] init];
    
    for(Human* human in arr2) {
        [humanResultArray addObject:human.name];
        [humanResultArray addObject:human.gender];
    }
    
    NSArray* arr3 = [self getAllObjectsWithClassName:@"Device" AndContext:context3];
    NSMutableArray* deviceResultArray = [[NSMutableArray alloc] init];
    
    for(Device* device in arr3) {
        [deviceResultArray addObject:device.type];
        [deviceResultArray addObject:device.model];
    }
    

    
    NSLog(@"Finish");
    
    
}

@end
