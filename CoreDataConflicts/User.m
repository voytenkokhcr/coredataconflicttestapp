//
//  User.m
//  CoreDataConflicts
//
//  Created by Войтенко Вадим on 13.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import "User.h"

@implementation User

@dynamic fullName;
@dynamic email;
@dynamic phoneNumber;

@end
