//
//  UserViewController.h
//  CoreDataConflicts
//
//  Created by Войтенко Вадим on 13.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface UserViewController : UITableViewController

@property(weak,nonatomic) NSString* userName;

@end
