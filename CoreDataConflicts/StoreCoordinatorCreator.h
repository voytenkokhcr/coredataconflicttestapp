//
//  StoreCoordinatorCreator.h
//  CoreDataConflicts
//
//  Created by Войтенко Вадим on 14.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface StoreCoordinatorCreator : NSObject

+ (NSPersistentStoreCoordinator *) getStoreCoordinatorWithModelName:(NSString *)modelName;

@end
