//
//  Device.h
//  CoreDataConflicts
//
//  Created by Войтенко Вадим on 15.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface Device : NSManagedObject

@property(strong,nonatomic) NSString* type;
@property(strong,nonatomic) NSString* model;

@end
