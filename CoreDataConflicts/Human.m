//
//  Human.m
//  CoreDataConflicts
//
//  Created by Войтенко Вадим on 15.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import "Human.h"

@implementation Human

@dynamic name;
@dynamic gender;

@end
