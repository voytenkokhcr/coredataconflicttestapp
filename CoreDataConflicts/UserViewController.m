//
//  UserViewController.m
//  CoreDataConflicts
//
//  Created by Войтенко Вадим on 13.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import "UserViewController.h"
#import "AppDelegate.h"

@interface UserViewController ()
@property (weak, nonatomic) IBOutlet UITextField *fullName;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *addUserButton;
@property (strong, nonatomic) User *user;

@property (strong, nonatomic) NSManagedObjectContext* context;

@end

@implementation UserViewController

-(void)viewWillAppear:(BOOL)animated {
    if(self.userName != nil && ![self.userName isEqualToString:@""]) {
        
        self.user =  [self getUserWithName:self.userName];
        self.fullName.text = self.user.fullName;
        self.email.text = self.user.email;
        self.phoneNumber.text = self.user.phoneNumber;
        [self.addUserButton setTitle:@"Edit" forState:UIControlStateNormal];

    }
    

}

-(void) viewWillDisappear:(BOOL)animated {
    [self.context refreshObject:self.user mergeChanges:NO];
//    self.userName = nil;
    self.user = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initMoc];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions

- (IBAction)addUser:(id)sender {
    
    User* user = nil;
    if(self.userName != nil && ![self.userName isEqualToString:@""]) {
        self.user.fullName = self.fullName.text;
        self.user.email = self.email.text;
        self.user.phoneNumber = self.phoneNumber.text;
    }
    else {
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:self.context];
        user.fullName = self.fullName.text;
        user.email = self.email.text;
        user.phoneNumber = self.phoneNumber.text;
    }
    if(![self.context save:nil]) {
        NSLog(@"Save fail!!!!!11111");
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddedSomeChangesToUser" object:nil];
    
    [self performSegueWithIdentifier:@"backToAllUsersSegue" sender:self];
}

#pragma mark - init

-(void) initMoc {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [self.context setPersistentStoreCoordinator:appDelegate.persistentStoreCoordinator];
    
    
}

-(User*) getUserWithName:(NSString *)name {
    
    NSString* pridicateString = [NSString stringWithFormat: @"fullName == '%@'",name];
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"User" inManagedObjectContext:self.context]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:pridicateString];
    [request setPredicate:predicate];
    
    return [[self.context executeFetchRequest:request error:nil] objectAtIndex:0];
}

#pragma mark - notifications



@end
