//
//  User.h
//  CoreDataConflicts
//
//  Created by Войтенко Вадим on 13.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface User : NSManagedObject

@property(strong,nonatomic) NSString* fullName;
@property(strong,nonatomic) NSString* email;
@property(strong,nonatomic) NSString* phoneNumber;

@end
