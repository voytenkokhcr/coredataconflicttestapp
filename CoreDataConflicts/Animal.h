//
//  Animal.h
//  CoreDataConflicts
//
//  Created by Войтенко Вадим on 15.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface Animal : NSManagedObject

@property(strong,nonatomic) NSString* name;
@property(strong,nonatomic) NSString* desc;

@end
