//
//  StoreCoordinatorCreator.m
//  CoreDataConflicts
//
//  Created by Войтенко Вадим on 14.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import "StoreCoordinatorCreator.h"

@implementation StoreCoordinatorCreator

+ (NSPersistentStoreCoordinator*) getStoreCoordinatorWithModelName:(NSString *)modelName {
    
    static NSMutableDictionary *storeCoordinators = nil;
    
    if( storeCoordinators == nil) {
        storeCoordinators = [[NSMutableDictionary alloc] init];
    }
    else {
        NSPersistentStoreCoordinator* coordinator = storeCoordinators[modelName];
        if(coordinator != nil) {
            return coordinator;
        }
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:modelName withExtension:@"momd"];
    NSManagedObjectModel* managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    NSString* SQLiteDBPathComponent = [NSString stringWithFormat:@"CoreTestDB%@.sqlite", [modelName substringFromIndex: modelName.length - 1]];
    
    NSURL *storeURL = [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] URLByAppendingPathComponent: SQLiteDBPathComponent];

    NSPersistentStoreCoordinator* coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:managedObjectModel];
    
    storeCoordinators[modelName] = coordinator;
    
    [coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:nil];
    return coordinator;
}

@end
