//
//  Device.m
//  CoreDataConflicts
//
//  Created by Войтенко Вадим on 15.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import "Device.h"

@implementation Device

@dynamic type;
@dynamic model;

@end
