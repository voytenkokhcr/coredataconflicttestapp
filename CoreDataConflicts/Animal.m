//
//  Animal.m
//  CoreDataConflicts
//
//  Created by Войтенко Вадим on 15.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import "Animal.h"

@implementation Animal

@dynamic name;
@dynamic desc;

@end
